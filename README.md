# Arborean Apparel


![<sub>update when?</sub>](https://i.imgur.com/YNBcrI5.pngg)


- [Arborean Apparel](#arborean-apparel)
    - [How to:](#how-to)
    - [Usage:](#usage)
      - [Updating:](#updating)
      - [Online:](#online)
  - [FAQ/Errors:](#faqerrors)
  - [Todo:](#todo)
  - [Known issues:](#known-issues)
  - [Update log](#update-log)




***
### How to:

- Download and install [TERA Toolbox](https://discord.gg/dUNDDtw)
- Download the module by either:
  - Clicking the download button next to "clone" and selecting the format (any will do), then extract
  - Creating a folder called `arborean-apparel`, then place [`modules.json`](https://gitlab.com/neverknowsbest/arborean-apparel/-/raw/master/module.json) inside
  - Cloning the repository with `git clone https://gitlab.com/neverknowsbest/arborean-apparel`
- Place the module's folder inside `<TeraToolbox>/Mods`
- Start Toolbox!

I also recommend checking the options in `config.json`.

### Commands:
Switch to toolbox channel by writing `/8` or `/toolbox` in chat, then hit space. You can also input those commands in a normal channel by using ! as prefix (example `!aa open`)

Use either `aa` or `u` (alias for Unicast users) for running the commands.

| Command                     | Description                       |
|-----------------------------|-----------------------------------|
| `aa open` or `aa ui`        | Opens the interface               |
| `aa idle`                   | Enable/disable idle animations    |
| `aa cb` or `aa crystalbind` | Enable/disable crystalbind effect |
| `aa reload`                 | Reloads the module                |

#### Updating:
This mod will be updated every time you start Toolbox, unless you disabled auto update.

#### Online:
By default Apparel will not share your costume selections with other people using the module around you by connecting to an external server. To enable, simply edit config.json and change `online: false` to `true` and `serverHost: ""` with a working host address.

The costume sharing server is bundled with the module if you wanna host it yourself.
****
## Q&A/Errors:

`The UI is invisible!` Try turning `transparent` to false. Some users with older operating systems may experience this bug.

`Transparency does not work!` Set `transparent` to true. If you're on Linux, use a compositor like [picom](https://github.com/yshui/picom) or whatever your WM/DE uses.

`Your version of Node.JS is too old to run TERA Toolbox. Version x.0.0 or newer is required.` Unless you're running Toolbox CLI with a separate install of Node.JS, this error should not appear. Either update Node or launch Toolbox with one of the executables located inside its folder.

`TypeError: electron.BrowserWindow is not a constructor or cannot find module 'Electron'`: Make sure you are running Toolbox GUI instead of CLI.

`The system cannot find the path specified.`: You don't have electron installed in tera-toolbox/node_modules (should be bundled by default). Try reinstalling Toolbox or downloading a compatible version of electron.

`Script doesn't work!` Please make sure you're using an updated version and you have read the README. Or maybe I screwed up something ¯\\_(ツ)_/¯ (please keep in mind i'm changing stuff once in a while, breakage may happen)

`Electron.exe is not compatible with this version of Windows` Please download the electron prebuilt for your OS [here](https://github.com/electron/electron/releases) and extract it into `tera-toolbox/node_modules/electron/dist` over the top of the existing one.

`How do I change back to my actual equipment?` On the GUI, click on the icons under "Equip". Running `aa reset` does not change it automatically yet.

`Where are the icons?` You can either extract from the game or downlaod them [here](https://github.com/hailstorming/tera-Icon-Dump), then place `icon_equipments`, `icon_items`, `icon_skills` and `icon_status` inside `www/img`.

***
## Todo:
- Do hat stuff
- Remove or fix emotes
- Add effect/slider saving
- Add more effects
- Make code less bad
- Add race/appearance changing
- Tidy up UI
- Add more features
- Use in-game awesomium instead of a separate electron window for the UI, or add an option to toggle between both (for some reason)
- Fix reset
- Add option to toggle between locally and remote hosted icons?
- other stuff
 
***
### Known issues:
- Running `aa reset` does not change back to original equipment, neither resets your options.
- Some spoofed mounts may prevent you from unmounting, requiring a relog.
- Unrestricted mount spoofing. This means the user can change a ground-only mount to a flight one and be able to fly, which can be a problem.
- Some temporary changers are completely ignored while the module is active.
***

## Update log:

### May 30 2022
- Hot reload support! This will reload the whole module instead of config.json only. Reload with `aa reload`
- Added "u" as command and "u ui" for opening the GUI
- Added CTRL+Shift+U as keybind for opening the GUI
  - For those too used with Unicast :)
- Fixed UI transparency

### May 20 2022
- Added scripts from original Arborean Apparel repo
  - dump-images: Dump icons from the game ( I don't use it and didn't modify either, but it's there anyway ¯\\_(ツ)_/¯)
  - dump-items: Generate items.json from a datacenter dump in json format. Also applied changes by codeagon
- Updated items.json for v92.04

### May 18 2022
- Remove contextIsolation warning that appears in Toolbox log every time you open the UI
- Added toggle for electron devtools in config.json
- Added `aa reload` for reloading the config

### May 14 2022
- Update definitions to match v92 (other versions are not supported)
- Remove deprecated isClassic call - no more annoying warnings in console
- Disable online mode by default and leave the server address blank
- Switch back to locally hosted images (it still used SaltyMonkey's old domain)
    - Please keep in mind that I am **not** including any assets, you will have to extract them yourself or download from here: [hailstorming/TERA-Icon-Dump](https://github.com/hailstorming/tera-Icon-Dump)
- I am not sure if I will keep this section updated, but if in doubt, please refer to the commit history.
### May 07 2022
- Revert deprecation
### December 19
- Saving changers
- Deleted releases, preparation to add to proxy GUI download list
### November 2
- Images work again btw
- I'm only updating this because I had something else to fix in the readme
- For a more consistent update log look at github commit history 
### Jun 18
- Images now loaded from server provided by [SaltyMonkey](https://github.com/SaltyMonkey/), you can delete img.asar from the /www folder now.
- Fixed several minor issues in regards to the UI.
- Added `skyEveryMap`to the config, allowing the selected sky preset to be applied every time a new map is loaded.
